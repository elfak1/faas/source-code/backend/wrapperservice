import lombok.RequiredArgsConstructor;
import lombok.ToString;

public class TesttC23lass {

    @RequiredArgsConstructor
    @ToString
    private static class TestPojo {

        private final Integer intValue;

        private final String strValue;

        private final boolean boolValue;
    }

    public String main(Integer intValue ,Boolean boolValue,String strValue ){
                  System.out.println("arg 0 : " + intValue);
                  System.out.println("arg 1 : " + strValue);
                  System.out.println("arg 2 : " + boolValue);

                  final TestPojo testPojo = new TestPojo(intValue, strValue, boolValue);
                  return testPojo.toString();
              };
}


//
//import org.joda.time.LocalTime;
//
//public class TesttC23lass {
//
//    public String main(String arg1 ){
//        LocalTime currentTime = new LocalTime();
//        return arg1 + currentTime;
//    };
//}