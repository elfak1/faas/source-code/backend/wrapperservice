package com.Diplomski.WrapperService.Handlers.Exceptions;

import com.Diplomski.WrapperService.Handlers.Exceptions.base.HandledException;
import org.springframework.http.HttpStatus;

public class BadRequestException extends HandledException {

    public BadRequestException(final String errorMessage) {
        super(errorMessage, HttpStatus.BAD_REQUEST);
    }
}
