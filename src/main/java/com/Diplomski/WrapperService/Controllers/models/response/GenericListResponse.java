package com.Diplomski.WrapperService.Controllers.models.response;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class GenericListResponse<T> extends BaseResponse {

    private final List<T> data;

    public GenericListResponse(List<T> data) {
        if (data == null)
            data = new ArrayList<>();
        this.data = data;
    }

    public GenericListResponse(List<T> data, Integer statusCode, String error, String message) {
        super(statusCode, error, message);
        this.data = data;
    }
}